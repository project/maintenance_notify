 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The module can send notifications when the website
is going on/off to/from maintenance mode.

If the site goes on/off into maintenance mode from some user,
the notifications (if enabled) are sent in the same time.

If the maintenance mode has been set using the `drush` command,
the notifications are sent in the next cron run.

Currently, Slack and email (multiple recipients) are supported.

For Slack notifications you need to enable and configure
the Slack module: https://www.drupal.org/project/slack

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/maintenance_notify


INSTALLATION
------------

Install the Maintenance Notify module as you would normally
install a contributed Drupal module.


CONFIGURATION
-------------

1. Navigate to `Administration > Extend` and enable the module.
2. Navigate to `Configuration > Development > Maintenance mode`
  and click on the `Notifications` tab.
3. Enable the notifications, configure the email recipients
  and/or enable Slack notifications.



MAINTAINERS
-----------

 * Dimitris Spachos (dspachos) - https://www.drupal.org/u/dspachos
