<?php

namespace Drupal\maintenance_notify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\maintenance_notify\MaintenanceNotifyService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Utility\EmailValidatorInterface;

/**
 * Configure Maintenance notify settings for this site.
 */
class MaintenanceModeSettingsForm extends ConfigFormBase {

  /**
   * The notifier service.
   *
   * @var \Drupal\maintenance_notify\MaintenanceNotifyService
   */
  protected $notifier;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Constructs the object.
   *
   * @param \Drupal\maintenance_notify\MaintenanceNotifyService $notifier_service
   *   The notifier service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   */
  public function __construct(
    MaintenanceNotifyService $notifier_service,
    ModuleHandlerInterface $module_handler,
    EmailValidatorInterface $email_validator
    ) {
    $this->notifier = $notifier_service;
    $this->moduleHandler = $module_handler;
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('maintenance_notify.service'),
      $container->get('module_handler'),
      $container->get('email.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'maintenance_notify_maintenance_mode_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['maintenance_notify.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('maintenance_notify.settings');
    $enabled = (bool) $config->get('enabled');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable maintenance mode on/off notifications'),
      '#default_value' => $enabled,
      '#description' => $this->t('If enabled, system will send notifications to specific users and/or Slack channel when maintenance mode goes on/off.'),
    ];

    $form['maintenance_notify_container'] = [
      '#type' => 'container',
    ];

    $form['maintenance_notify_container']['email_notifications'] = [
      '#type' => 'details',
      '#title' => $this->t('Email notifications'),
      '#group' => 'email_settings',
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':checkbox[name="enabled"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['maintenance_notify_container']['email_notifications']['email_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Send email to:'),
      '#description' => $this->t('Send the email to the specified address. You can add more than one emails by separating addresses with comma.'),
      '#default_value' => $config->get('email_to') ?? '',
      '#attributes' => [
        'placeholder' => t('Email(s)'),
        'multiple' => 'multiple',
      ],
      '#limit_validation_errors' => [],
    ];

    $form['maintenance_notify_container']['slack_notifications'] = [
      '#type' => 'details',
      '#title' => $this->t('Slack notifications'),
      '#group' => 'slack_settings',
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':checkbox[name="enabled"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $slack_settings = $this->config('slack.settings');
    if ($this->moduleHandler->moduleExists('slack')) {

      $is_slack_configured = $slack_settings->get('slack_webhook_url') ?: NULL;
      if (!$is_slack_configured) {
        $this->displaySlackWarning();
      }

      $form['maintenance_notify_container']['slack_notifications']['slack'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Send the notification to the %channel Slack channel.', [
          '%channel' => $slack_settings->get('slack_channel'),
        ]),
        '#description' => $this->t('Channel can be configured in the Slack %settings page.', [
          '%settings' => $link = Link::fromTextAndUrl(t('settings'), Url::fromRoute('slack.admin_settings'))->toString(),
        ]),
        '#default_value' => $config->get('slack') ?? FALSE,
        '#attributes' => [
          'disabled' => !$is_slack_configured,
        ],
      ];
    }
    else {
      $link = $this->getSlackModuleLink();
      $form['maintenance_notify_container']['slack_notifications']['slack_message'] = [
        '#markup' => '<p>' . $this->t('Install and configure the slack module to send notifications via slack.') . $link . '</p>',
      ];
      $form['maintenance_notify_container']['slack_notifications']['slack'] = [
        '#type' => 'hidden',
        '#default_value' => FALSE,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $email_to = $values['email_to'];
    if (!empty($email_to)) {
      $emails = explode(',', $email_to);
      $emails = array_map(function ($item) {
        return $this->emailValidator->isValid(trim($item)) && filter_var(trim($item), FILTER_VALIDATE_EMAIL);
      }, $emails);
      if (in_array(FALSE, $emails)) {
        $invalid_count = count($emails) - array_sum($emails);
        $error = $this->formatPlural($invalid_count,
          'One email address is not valid.',
          'More than one email addresses are not valid');
        $form_state->setErrorByName('email_to', $error);
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('maintenance_notify.settings');
    $config
      ->set('enabled', $values['enabled'])
      ->set('email_to', trim($values['email_to']))
      ->set('slack', $values['slack'])
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Displays a warning if Slack is not properly configyred.
   */
  private function displaySlackWarning() {
    $url = Url::fromRoute('slack.admin_settings');
    $message = $this->t('NOTICE: Slack module is enabled, but it is not properly configured.');
    $message .= "<a href='{$url->toString()}'><em>{$this->t('Configure Slack')}</em></a>.";
    $this->messenger()->addWarning(Markup::create($message));
  }

  /**
   * Get the contributed Slack module URL.
   *
   * @return string
   *   The URL to the contributed Slack module.
   */
  private function getSlackModuleLink() {
    $url = Url::fromUri('https://www.drupal.org/project/slack');
    $url->setOptions([
      'attributes' => [
        'target' => '_blank',
      ],
    ]);
    return Link::fromTextAndUrl($this->t('Slack module'), $url)->toString();
  }

}
