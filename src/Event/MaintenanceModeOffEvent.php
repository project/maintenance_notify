<?php

namespace Drupal\maintenance_notify\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Wraps a maintenance off switch event for event listeners.
 */
class MaintenanceModeOffEvent extends Event {

  public const MAINTENANCE_NOTIFY_OFF = 'maintenance_notify.off';

  /**
   * The event options. see comments bellow.
   *
   * @var array
   */
  protected $options;

  /**
   * Constructs the event object.
   *
   * @param array $options
   *   An array with options for the event. These options are:
   *   status: the current status of the maintenance mode (0 or 1)
   *   user: the user (uid) that triggered the maintenance mode switch.
   *   message: a message contains the maintenance mode message
   *   and information about the user. If the switch is made using drush
   *   the user value is set to 0.
   */
  public function __construct(array $options) {
    $this->options = $options;
  }

  /**
   * Get the message.
   *
   * @return array
   *   The options for the current instance.
   */
  public function getOptions(): array {
    return $this->options;
  }

}
