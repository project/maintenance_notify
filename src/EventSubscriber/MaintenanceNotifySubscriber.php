<?php

namespace Drupal\maintenance_notify\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\maintenance_notify\Event\MaintenanceModeOnEvent;
use Drupal\maintenance_notify\Event\MaintenanceModeOffEvent;
use Drupal\maintenance_notify\MaintenanceNotifyService;

/**
 * Maintenance notify event subscriber.
 */
class MaintenanceNotifySubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The notifier service.
   *
   * @var \Drupal\maintenance_notify\MaintenanceNotifyService
   */
  protected $notifier;

  /**
   * Constructs a MaintenanceNotifySubscriber object.
   *
   * @param \Drupal\maintenance_notify\MaintenanceNotifyService $notifier_service
   *   The notifier service.
   */
  public function __construct(
    MaintenanceNotifyService $notifier_service
    ) {
    $this->notifier = $notifier_service;
  }

  /**
   * Callback for the MaintenanceModeOnEvent event.
   *
   * @param \Drupal\maintenance_notify\Event\MaintenanceModeOnEvent $event
   *   The event to process.
   */
  public function onMaintenanceModeOn(MaintenanceModeOnEvent $event) {
    $msg = $this->notifier->getMessage($event->getOptions());
    $this->notifier->doSend($msg->render());
  }

  /**
   * Callback for the MaintenanceModeOffEvent event.
   *
   * @param \Drupal\maintenance_notify\Event\MaintenanceModeOffEvent $event
   *   Response event.
   */
  public function onMaintenanceModeOff(MaintenanceModeOffEvent $event) {
    $msg = $this->notifier->getMessage($event->getOptions());
    $this->notifier->doSend($msg->render());
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MaintenanceModeOnEvent::MAINTENANCE_NOTIFY_ON][] = ['onMaintenanceModeOn'];
    $events[MaintenanceModeOffEvent::MAINTENANCE_NOTIFY_OFF][] = ['onMaintenanceModeOff'];
    return $events;
  }

}
