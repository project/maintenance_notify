<?php

namespace Drupal\maintenance_notify;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\maintenance_notify\Event\MaintenanceModeOnEvent;
use Drupal\maintenance_notify\Event\MaintenanceModeOffEvent;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Service description.
 */
class MaintenanceNotifyService {


  use StringTranslationTrait;

  public const MAINTENANCE_MODE_NO_SWITCH = 0;
  public const MAINTENANCE_MODE_SWITCHED_ON = 1;
  public const MAINTENANCE_MODE_SWITCHED_OFF = 2;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Mail\MailManagerInterface definition.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * An event dispatcher instance to use for configuration events.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a MaintenanceNotifyService object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The date formatter.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The date formatter.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   An event dispatcher instance for use.
   */
  public function __construct(
    MessengerInterface $messenger,
    AccountInterface $account,
    EntityTypeManagerInterface $entity_type_manager,
    StateInterface $state,
    ConfigFactoryInterface $config_factory,
    MailManagerInterface $mail_manager,
    LanguageManagerInterface $language_manager,
    ModuleHandlerInterface $module_handler,
    LoggerChannelFactoryInterface $logger_factory,
    EventDispatcherInterface $event_dispatcher
    ) {
    $this->messenger = $messenger;
    $this->account = $account;
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
    $this->configFactory = $config_factory;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;
    $this->loggerFactory = $logger_factory;
    $this->dispatcher = $event_dispatcher;
  }

  /**
   * Send notifications when maintenance mode has switched.
   *
   * @param int $status
   *   The switched status.
   */
  public function sendNotifications(int $status = self::MAINTENANCE_MODE_SWITCHED_ON) {
    $options['status'] = $status;
    $options['uid'] = $this->account->id();
    $maintenance_message = $this->configFactory->get('system.maintenance')->get('message');
    $message = new FormattableMarkup($maintenance_message, [
      '@site' => $this->configFactory->get('system.site')->get('name'),
    ]);
    $options['message'] = $message->__toString();

    if ($this->isMaintenanceModeNotificationsEnabled()) {
      if ($status === self::MAINTENANCE_MODE_SWITCHED_ON) {
        $event = new MaintenanceModeOnEvent($options);
        $this->dispatcher->dispatch(MaintenanceModeOnEvent::MAINTENANCE_NOTIFY_ON, $event);
      }
      elseif ($status === self::MAINTENANCE_MODE_SWITCHED_OFF) {
        $event = new MaintenanceModeOffEvent($options);
        $this->dispatcher->dispatch(MaintenanceModeOffEvent::MAINTENANCE_NOTIFY_OFF, $event);
      }

      // Reset the previous state.
      $current_state = $this->state->get('system.maintenance_mode');
      $this->state->set('system.maintenance_mode.previous_state', $current_state);
    }
  }

  /**
   * Dispatch the notification events.
   *
   * @param string $message
   *   The notification message (email body).
   *
   * @return bool
   *   True if email sent successfully,
   *   false if at least one email has failed.
   */
  public function sendEmailNotifications(string $message): bool {
    $emails = $this->getEmails();
    $sent = [];
    foreach ($emails as $email) {
      $module = 'maintenance_notify';
      $key = 'maintenance_notify';
      $to = trim($email);
      $params['subject'] = $this->t('@site maintenance mode switch notification', [
        '@site' => $this->configFactory->get('system.site')->get('name'),
      ])->render();
      $params['message'] = $message;
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
      $result = $this->mailManager->mail($module, $key, $to, $langcode, $params, NULL, TRUE);
      $sent[] = (bool) $result['result'];
    }
    // @todo Add some logging here based on send results.
    return TRUE;
  }

  /**
   * Sends Slack notification.
   *
   * @param string $message
   *   The notification message (Slack).
   *
   * @return bool
   *   True if email sent successfully, false otherwise.
   */
  public function sendSlackNotifications(string $message): bool {
    $response = \Drupal::service('slack.slack_service')->sendMessage($message);
    return ($response && Response::HTTP_OK == $response->getStatusCode());
  }

  /**
   * Log the maintenance action in system log.
   *
   * @param string $message
   *   The log message.
   */
  public function logMaintenanceModeAction(string $message) {
    $this->loggerFactory->get('maintenance_modify')->notice($message);
  }

  /**
   * Check if maintenance mode is turned on or off.
   *
   * @return int
   *   Returns the switched status of the maintenance mode.
   */
  public function checkMaintenanceModeSwitch(): int {
    $previous_state = $this->state->get('system.maintenance_mode.previous_state');
    $current_state = $this->state->get('system.maintenance_mode');
    $switched = $previous_state !== NULL
      && $current_state !== NULL
      && $previous_state !== $current_state;

    if ($switched) {
      return $current_state ? self::MAINTENANCE_MODE_SWITCHED_ON : self::MAINTENANCE_MODE_SWITCHED_OFF;
    }

    return self::MAINTENANCE_MODE_NO_SWITCH;
  }

  /**
   * Get users to email.
   *
   * @return array
   *   An array with user emails to notify.
   */
  private function getEmails(): array {
    $emails = $this->configFactory->get('maintenance_notify.settings')->get('email_to');
    return explode(',', $emails);
  }

  /**
   * Check if notifications are enabled.
   *
   * @return bool
   *   True if notifier is enabled, false otherwise.
   */
  private function isMaintenanceModeNotificationsEnabled(): bool {
    return (bool) $this->configFactory->get('maintenance_notify.settings')->get('enabled');
  }

  /**
   * Construct message to send.
   *
   * @param array $options
   *   An array with options for the event. These options are:
   *   status: the current status of the maintenance mode (0 or 1)
   *   user: the user (uid) that triggered the maintenance mode switch.
   *   message: a message contains the maintenance mode message
   *   and information about the user. If the switch is made using drush
   *   the user value is set to 0.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The notification message that will be sent
   *   to the user (email body & Slack).
   */
  public function getMessage(array $options): TranslatableMarkup {
    $site = $this->configFactory->get('system.site')->get('name');
    $messages = [
      MaintenanceNotifyService::MAINTENANCE_MODE_SWITCHED_ON => $this->t('Notice: the site @site is in maintenance mode.', [
        '@site' => $site,
      ]),
      MaintenanceNotifyService::MAINTENANCE_MODE_SWITCHED_OFF => $this->t('Maintenance mode in @site turned off. The website is now back to live! 🎉', [
        '@site' => $site,
      ]),
    ];

    if ($options['uid']) {
      $user = User::load($options['uid']);
      if ($user instanceof UserInterface) {
        $name = ucfirst($user->getDisplayName());
        $messages = [
          MaintenanceNotifyService::MAINTENANCE_MODE_SWITCHED_ON => $this->t('@name put the site @site in maintenance mode. 🛑', [
            '@name' => $name,
            '@site' => $site,
          ]),
          MaintenanceNotifyService::MAINTENANCE_MODE_SWITCHED_OFF => $this->t('@name turned off maintenance mode. @site is now back to live! 🎉', [
            '@name' => $name,
            '@site' => $site,
          ]),
        ];
      }
    }
    return $messages[$options['status']];
  }

  /**
   * Fire up the actual sending, based on config values.
   *
   * @param string $message
   *   The notification message that will be sent
   *   to the user (email body & Slack).
   */
  public function doSend(string $message): void {
    if ($this->shouldSendEmailNotification()) {
      $this->sendEmailNotifications($message);
      // @todo Include emails in message
      $this->messenger->addMessage($this->t('Email notifications sent!'));
    }
    if ($this->shouldSendSlackNotification()
      && $this->moduleHandler->moduleExists('slack')) {
      $this->sendSlackNotifications($message);
      // @todo Include channel in message
      $this->messenger->addMessage($this->t('Slack notifications sent!'));
    }
    $this->logMaintenanceModeAction($message);
  }

  /**
   * Check if system should send email notifications.
   *
   * @return bool
   *   True if should send, false otherwise.
   */
  private function shouldSendEmailNotification() {
    return !empty($this->configFactory->get('maintenance_notify.settings')->get('email_to'));
  }

  /**
   * Check if system should send slack notifications.
   *
   * @return bool
   *   True if should send, false otherwise.
   */
  private function shouldSendSlackNotification() {
    $settings = $this->configFactory->get('maintenance_notify.settings');
    return (bool) $settings->get('slack');
  }

  /**
   * Checks if maintenance mode has switched from on/off.
   */
  public function checkMode() {
    $status = $this->checkMaintenanceModeSwitch();
    if ($status == MaintenanceNotifyService::MAINTENANCE_MODE_SWITCHED_ON ||
      $status == MaintenanceNotifyService::MAINTENANCE_MODE_SWITCHED_OFF) {
      $this->sendNotifications($status);
    }
  }

}
